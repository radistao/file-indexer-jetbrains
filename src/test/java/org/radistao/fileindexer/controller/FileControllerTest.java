package org.radistao.fileindexer.controller;

import org.junit.jupiter.api.Test;
import org.radistao.fileindexer.service.FileIndexerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedHashSet;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class FileControllerTest {

    // TODO: more tests for corner cases

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FileIndexerService fileIndexerService;

    @Test
    void uploadFile_shouldFileIndexService() throws Exception {
        final var multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Test words\n to index 22".getBytes());

        given(fileIndexerService.indexMultipartFile(multipartFile, multipartFile.getOriginalFilename()))
                .willReturn(5);

        mvc.perform(multipart("/file/upload").file(multipartFile))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"fileName\": \"test.txt\"," +
                        "\"uniqueWordsCount\": 5}", false));

        then(fileIndexerService).should().indexMultipartFile(multipartFile, multipartFile.getOriginalFilename());
    }

    @Test
    void findFilesContaining_shouldCallFileIndexService() throws Exception {
        // because JSON doesn't have sets but lists we need to preserve order in the returned set
        final var orderedSet = new LinkedHashSet<>(asList("abc.txt", "ggg.pdf"));

        given(fileIndexerService.findFilesContaining("woot"))
                .willReturn(orderedSet);

        mvc.perform(get("/file/find-word")
                .param("word", "woot"))
                .andExpect(status().isOk())
                .andExpect(content().json("[\"abc.txt\", \"ggg.pdf\"]", false));

        then(fileIndexerService).should().findFilesContaining("woot");
    }
}