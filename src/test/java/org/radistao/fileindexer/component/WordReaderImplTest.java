package org.radistao.fileindexer.component;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class WordReaderImplTest {
    private WordReaderImpl wordReader = new WordReaderImpl();

    @Test
    void splitToWords() {
        assertThat(wordReader.splitToWords("")).containsExactly("");
        assertThat(wordReader.splitToWords("abc")).containsExactly("abc");
        assertThat(wordReader.splitToWords("abc def")).containsExactly("abc", "def");
        assertThat(wordReader.splitToWords("abc def\tвася\nn'etes"))
                .containsExactly("abc", "def", "вася", "n'etes");
    }

    @Test
    void toLowerCase() {
        assertThat(wordReader.toLowerCase("")).isEqualTo("");
        assertThat(wordReader.toLowerCase("abc")).isEqualTo("abc");
        assertThat(wordReader.toLowerCase("ABC")).isEqualTo("abc");
        assertThat(wordReader.toLowerCase("JHgJhgJhg")).isEqualTo("jhgjhgjhg");
        assertThat(wordReader.toLowerCase("ЮниКод")).isEqualTo("юникод");
    }

    @Test
    void stripAndLowerCase() {
        assertThat(wordReader.stripAndLowerCase(" ")).isEqualTo("");
        assertThat(wordReader.stripAndLowerCase("\t\n")).isEqualTo("");
        assertThat(wordReader.stripAndLowerCase(" abc\t")).isEqualTo("abc");
        assertThat(wordReader.stripAndLowerCase("\n\n\nABC     ")).isEqualTo("abc");
        assertThat(wordReader.stripAndLowerCase("\n\fJHgJhgJhg \t")).isEqualTo("jhgjhgjhg");
        assertThat(wordReader.stripAndLowerCase("ЮниКод")).isEqualTo("юникод");
        assertThat(wordReader.stripAndLowerCase("\u2000\u2009ЮниКод\u2028\u2029")).isEqualTo("юникод");
    }
}