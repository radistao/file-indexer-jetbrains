package org.radistao.fileindexer.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.radistao.fileindexer.component.WordReader;
import org.radistao.fileindexer.component.WordsIndexingFactory;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FileIndexerServiceImplTest {

    // TODO: more tests for indexMultipartFile() etc

    @Mock
    private WordReader wordReader;

    @Mock
    private WordsIndexingFactory wordsIndexingFactory;

    @InjectMocks
    private FileIndexerServiceImpl fileIndexerService;

    @Test
    void findFilesContaining_callsWordReaderAndFilesIndex() {
        final var wordToFind = " bAtmAn ";
        final var fileIndexMock = new HashMap<>(
                Map.of("batman", Map.of("fileOne", true, "file two.txt", true)));
        when(wordsIndexingFactory.createWordsStorage()).thenReturn(fileIndexMock);
        fileIndexerService = new FileIndexerServiceImpl(wordReader, wordsIndexingFactory);

        when(wordReader.stripAndLowerCase(wordToFind)).thenReturn("batman");
        final var foundFiles = fileIndexerService.findFilesContaining(wordToFind);
        assertThat(foundFiles).containsExactlyInAnyOrder("fileOne", "file two.txt");
        verify(wordReader).stripAndLowerCase(wordToFind);

        when(wordReader.stripAndLowerCase(any())).thenReturn("");
        final var emptyFiles = fileIndexerService.findFilesContaining("joker");
        assertThat(emptyFiles).isEmpty();

        verify(wordReader, times(2)).stripAndLowerCase(anyString());
    }
}