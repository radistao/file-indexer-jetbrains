package org.radistao.fileindexer.exception;

public class FileIndexingException extends RuntimeException {
    public FileIndexingException(final String message) {
        super(message);
    }

    public FileIndexingException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
