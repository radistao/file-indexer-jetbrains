package org.radistao.fileindexer.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

public interface FileIndexerService {
    int indexMultipartFile(MultipartFile file, String desiredName);

    Set<String> findFilesContaining(String word);
}
