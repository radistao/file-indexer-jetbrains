package org.radistao.fileindexer.service;

import org.radistao.fileindexer.component.WordReader;
import org.radistao.fileindexer.component.WordsIndexingFactory;
import org.radistao.fileindexer.exception.FileIndexingException;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;

@Component
public class FileIndexerServiceImpl implements FileIndexerService {

    private final WordReader wordReader;
    private final WordsIndexingFactory wordsIndexingFactory;
    private final Map<String, Map<String, Boolean>> filesIndex;

    public FileIndexerServiceImpl(final WordReader wordReader, final WordsIndexingFactory wordsIndexingFactory) {
        this.wordReader = wordReader;
        this.wordsIndexingFactory = wordsIndexingFactory;

        filesIndex = wordsIndexingFactory.createWordsStorage();
    }

    /**
     * <b>Implementation notes</b>:
     * <br/>
     * For concurrency reason separate words indexing for each uploaded file in the following steps:<ol>
     * <li>read text file lines in parallel mode using default Java Stream API. This uses default
     * {@link ForkJoinPool#commonPool()}, but could be extended in the future</li>
     * <li>split the line into words</li>
     * <li>map each word to lower case</li>
     * <li>collect words to unique set</li>
     * <li>merge collected words to {@link #filesIndex}</li>
     * </ol>
     * <p>
     * This approach allows to avoid locking {@link #filesIndex} on duplicate words, but only once per word in the file.
     *
     * @param file        file to parse
     * @param desiredName desired file name (in case it shall be not the same as
     *                    {@link MultipartFile#getOriginalFilename()}
     * @return number of unique parsed words for this file
     */
    @Override
    public int indexMultipartFile(final MultipartFile file, final String desiredName) {
        final var mapMergeFunction = wordsIndexingFactory.mapMergeFunctionForFileName(desiredName);

        try (final var br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            // collect words to unique set without blocking filesIndex
            final var uniqueWords = br.lines()
                    .parallel()
                    .flatMap(this::splitLineToWords)
                    .map(wordReader::toLowerCase)
                    .collect(toSet());

            // when all words are collected - update filesIndex.
            // This allows lock (synchronize) the filesIndex only on unique words, but not each word of the parsed file
            uniqueWords.parallelStream()
                    .forEach(uniqueWord -> filesIndex.compute(uniqueWord, mapMergeFunction));

            return uniqueWords.size();
        } catch (final IOException e) {
            throw new FileIndexingException(format("File \"%s\" can't be read: I/O exception.", desiredName), e);
        } catch (final Exception e) {
            // to avoid exposing internal errors to the service response
            final var message = format("File \"%s\" can't be read: unexpected internal exception.", desiredName);
            LoggerFactory.getLogger(this.getClass()).error(message, e);
            throw new FileIndexingException(message + " See the internal logs");
        }
    }

    @Override
    public Set<String> findFilesContaining(final String word) {
        final var strippedLowerWord = wordReader.stripAndLowerCase(word);
        return unmodifiableSet(filesIndex.getOrDefault(strippedLowerWord, emptyMap()).keySet());
    }

    private Stream<? extends String> splitLineToWords(final String line) {
        return Arrays.stream(wordReader.splitToWords(line));
    }
}
