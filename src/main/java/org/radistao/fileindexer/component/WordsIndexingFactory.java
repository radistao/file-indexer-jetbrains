package org.radistao.fileindexer.component;

import java.util.Map;
import java.util.function.BiFunction;

public interface WordsIndexingFactory {

    Map<String, Map<String, Boolean>> createWordsStorage();

    Map<String, Boolean> createWordFilesSet();

    BiFunction<String, Map<String, Boolean>, Map<String, Boolean>> mapMergeFunctionForFileName(String newFileName);
}
