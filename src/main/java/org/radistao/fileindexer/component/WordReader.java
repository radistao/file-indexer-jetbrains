package org.radistao.fileindexer.component;

public interface WordReader {

    String[] splitToWords(String line);

    String toLowerCase(String word);

    String stripAndLowerCase(String word);
}
