package org.radistao.fileindexer.component;

import org.springframework.stereotype.Component;

/**
 * Implementation for words parsing logic.
 *
 * <p>
 * Current implementation splits words by all known Unicode <i>non-alphabetic</i> and <i>non-digit</i> characters
 * according to <a href="https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/regex/Pattern.html">
 * Java 11 Patten specification (POSIX character classes)</a>
 * <br/>
 * See {@link #WORD_SEPARATOR} pattern.
 */
@Component
public class WordReaderImpl implements WordReader {

//    public static final String WORD_SEPARATOR = "\\W+";

    // with Unicode support + single quote (apostrophe words)
    public static final String WORD_SEPARATOR = "[^\\p{IsAlphabetic}\\p{IsDigit}']+";

    public String[] splitToWords(final String line) {
        return line.split(WORD_SEPARATOR);
    }

    /**
     * Default Java lower case converting ({@link String#toLowerCase()}.
     *
     * <p>
     * Based on future requirements this could be changed for some <i>special</i> alphabets, which have difficult
     * upper<->lower case converting (like Turkey, Greek, Georgian etc)
     *
     * @param word word to convert to lower case.
     * @return {@code word} in lower case
     * @see String#toLowerCase()
     * @see #stripAndLowerCase(String)
     */
    public String toLowerCase(final String word) {
        return word.toLowerCase();
    }

    /**
     * Default Java stripping (trimming with Unicode support) with lower case converting using {@link String#strip()}
     * and internal {@link #toLowerCase(String)}.
     *
     * <p>
     * Based on future requirements this could be changed for some additional white-space chars, which are not
     * defined in Java 11.
     *
     * @param word word to strip and convert to lower case.
     * @return {@code word} stripped and in lower case
     * @see String#strip()
     * @see #toLowerCase(String)
     */
    public String stripAndLowerCase(final String word) {
        return toLowerCase(word.strip());
    }
}
