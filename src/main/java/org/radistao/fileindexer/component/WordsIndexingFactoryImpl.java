package org.radistao.fileindexer.component;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

/**
 * Special factory for file index cache.
 *
 * <p>
 * This implementation uses default Java {@link ConcurrentHashMap} implementations as the most efficient available
 * in default JDK.
 */
@Component
public class WordsIndexingFactoryImpl implements WordsIndexingFactory {

    public ConcurrentHashMap<String, Map<String, Boolean>> createWordsStorage() {
        return new ConcurrentHashMap<>();
    }

    /**
     * Create a <i>Set</i> for storing file names.
     *
     * <p>
     * Concurrent Set is not available in JDK by default, and concurrent hash map doesn't accept null as value,
     * hence we use a workaround: Map with boolean (just <code>true</code>) values.
     *
     * @return new instance of {@link ConcurrentHashMap}
     */
    public ConcurrentHashMap<String, Boolean> createWordFilesSet() {
        return new ConcurrentHashMap<>();
    }

    /**
     * Function to merge file names for particular word.
     *
     * <p>
     * This function is intended to be used in {@link ConcurrentHashMap#compute(Object, BiFunction)} as it knows
     * how to perform merging of the values (file names).
     *
     * <p>
     * Because this factory produces WordsStorage and WordFilesSet - it knows better how to merge new files
     * to existent WordsStorage.
     * <br/>
     * According to {@link ConcurrentHashMap#compute} implementation the function will be performed in synchronized
     * block, thus it's safe to create a new instance usign {@link #createWordFilesSet()}.
     *
     * @param newFileName file name to add to the index for respective new word.
     * @return binary function which merges concurrent maps where key is word and value - file name.
     * @see ConcurrentHashMap#compute(Object, BiFunction)
     */
    public BiFunction<String, Map<String, Boolean>, Map<String, Boolean>>
    mapMergeFunctionForFileName(final String newFileName) {
        return (word, oldMap) -> {
            if (oldMap == null) {
                oldMap = createWordFilesSet();
            }

            oldMap.put(newFileName, true);
            return oldMap;
        };
    }
}
