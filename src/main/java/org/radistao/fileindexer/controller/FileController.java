package org.radistao.fileindexer.controller;

import org.radistao.fileindexer.exception.FileIndexingException;
import org.radistao.fileindexer.service.FileIndexerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptySet;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping(value = "/file", produces = APPLICATION_JSON_VALUE)
public class FileController {

    private final FileIndexerService fileIndexerService;

    public FileController(final FileIndexerService fileIndexerService) {
        this.fileIndexerService = fileIndexerService;
    }

    /**
     * @param file file to index
     * @return saved file name (could be different if full path was specified in
     * {@link MultipartFile#getOriginalFilename()} and number or unique indexed words in the {@code file}
     */
    // example: curl -v -F "file=@src/test/resources/war-and-peace.txt" localhost:8080/file/upload 2>/dev/null
    // for file in src/test/resources/* ; do printf "\nParsing \"$file\":\n" ; curl -v -F "file=@${file}" localhost:8080/file/upload 2>/dev/null ; printf "\n" ; done
    @PostMapping(value = "/upload", consumes = MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> upload(@RequestParam("file") final MultipartFile file) {
        var savedFileName = file.getOriginalFilename();
        if (savedFileName == null) {
            throw new FileIndexingException("Filename is empty");
        }
        // extract file name if contains path info, see MultipartFile#getOriginalFilename() doc for details
        var pathSeparatorIndex = savedFileName.lastIndexOf("/");
        if (pathSeparatorIndex < 0) {
            pathSeparatorIndex = savedFileName.lastIndexOf("\\");
        }

        if (pathSeparatorIndex >= 0) {
            savedFileName = savedFileName.substring(pathSeparatorIndex);
        }

        return Map.of("fileName", savedFileName,
                "uniqueWordsCount", fileIndexerService.indexMultipartFile(file, savedFileName));
    }

    /**
     * @param word search for files containing {@code word}.
     * @return Set(list) of files containing {@code word}. Returns empty set if no such files.
     */
    // example: curl -v localhost:8080/file/find-word?word=war     2>/dev/null
    //          curl -v localhost:8080/file/find-word?word=warship 2>/dev/null
    // for word in king god money lear "%D0%BA%D0%BE%D1%80%D0%BE%D0%BB%D1%8C%0A%20" "j%27y" "j%27t" "Napoleon" "l%27Empereur" "%D0%9D%D0%B0%D0%BF%D0%BE%D0%BB%D0%B5%D0%BE%D0%BD" ; do echo -e "\nSearchin \"${word}\":" ; curl "localhost:8080/file/find-word?word=${word}" 2>/dev/null ; echo "" ; done
    @GetMapping("/find-word")
    public Set<String> findWord(@RequestParam("word") final String word) {
        if (word == null || word.strip().isEmpty()) {
            return emptySet();
        }
        return fileIndexerService.findFilesContaining(word);
    }
}
