# File Indexer

Необходимо создать web сервис индексации текстовых файлов по словам.

Http API сервиса должен позволять добавлять в систему файлы
и выдавать список файлов содержащих заданное слово.
Сервис должен масштабироваться. Сервис должен быть высоко доступным.

Для реализации желательно использовать языки Kotlin или Java.
Не стоит широко использовать интеграции со сторонними сервисами,
библиотеками, частично или полностью реализующими ключевую функциональность.

Оценивается правильность работы программы, качество кода,
дизайн системы и API.
По спорным или не специфицированным вопросам примите решение самостоятельно.

## Implementation notes

The implementation is based on Java native
[ConcurrentHashMap](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/ConcurrentHashMap.html)
as the most efficient default Map implementation in JDK
for concurrent/distributed computations. For more details follow
[FileIndexerServiceImpl](/src/main/java/org/radistao/fileindexer/service/FileIndexerServiceImpl.java)
documentation and comments.

For more complex and distributed systems in the future the service
could be re-factored to word store indexes in distributed and scalable
computing and storage systems (like Apache Hadoop, Amazon ElastiCache,
Redis). But this is out of scope of this particular task.

## API Endpoints

See [openapi.yaml](/openapi.yaml) REST API description.

## Build and Run

**Build**:

  - `./gradlew clean build`

**Run**:

  - `./gradlew clean bootRun`

    or

  - `./gradlew clean bootJar && java -Dfile.encoding=UTF-8 -jar build/libs/file-indexer.jar`

## Tests

**All added Unit/Integration tests are incomplete:
they are added just as an example and _will be extended_
to all untested methods, corner cases etc.**

### Manual test running service

**Upload files from test `/resources`**:
```bash
for file in src/test/resources/* ; do \
    printf "\nParsing \"$file\":\n" ; \
    curl -v -F "file=@${file}" localhost:8080/file/upload 2>/dev/null ; \
    printf "\n" ; \
done
```

**Search for files containing words`**:
```bash
for word in "king" "God" "moNey" "lear" \
"%D0%BA%D0%BE%D1%80%D0%BE%D0%BB%D1%8C%0A%20" "j%27y" "j%27t" "Napoleon" \
"l%27Empereur" "%D0%9D%D0%B0%D0%BF%D0%BE%D0%BB%D0%B5%D0%BE%D0%BD" ; do \
    echo -e "\nSearchin \"${word}\":" ; \
    curl "localhost:8080/file/find-word?word=${word}" 2>/dev/null ; \
    echo "" ; \
done
```
