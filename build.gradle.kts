import org.gradle.api.tasks.testing.logging.TestLogEvent.*
import java.nio.charset.StandardCharsets

plugins {
    id("org.springframework.boot") version "2.2.6.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    java
}

repositories {
    jcenter()
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

val UTF_8 = StandardCharsets.UTF_8.name()

tasks {
    compileJava {
        options.encoding = UTF_8
    }
    compileTestJava {
        options.encoding = UTF_8
    }
    bootRun {
        jvmArgs = listOf("-Dfile.encoding=${UTF_8}")
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()

    testLogging {
        showExceptions = true
        showCauses = true
        events = setOf(PASSED, SKIPPED, FAILED)
    }
}
